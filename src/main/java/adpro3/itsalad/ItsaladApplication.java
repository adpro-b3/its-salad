package adpro3.itsalad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItsaladApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItsaladApplication.class, args);
	}

}
