package adpro3.itsalad.gamePage.core;

public class Score {
    private int score;

    public Score(){
        this.score = 0;
    }

    public int update(){
        return score++;
    }

    public int getScore(){
        return score;
    }
}
