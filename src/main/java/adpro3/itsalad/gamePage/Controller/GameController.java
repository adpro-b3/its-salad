package adpro3.itsalad.gamePage.Controller;

import adpro3.itsalad.gamePage.Service.DoneService;
import adpro3.itsalad.gamePage.core.CheckScore;
import adpro3.itsalad.gamePage.core.Score;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;

@Controller
public class GameController {

    @Autowired
    private DoneService dn;

    @RequestMapping(method = RequestMethod.GET, value = "/game")
    private String game(Model model){
        CheckScore scr = new CheckScore(new Score());
        model.addAttribute("scores", dn.getScore());
        return "gamePage";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/")
    @ResponseBody
    private String helloWorld() {
        return "Hello World";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/generateMenu")
    private String generateMenu() {
        return "generateMenu";
    }
    @RequestMapping(method = RequestMethod.GET, value = "/game-done")
    private String done() {
        dn.done();
        return "redirect:/game";
    }
    @RequestMapping(method = RequestMethod.GET, value = "/game-reset")
    private String reset() {
        return "reset";
    }


}
