package adpro3.itsalad.gamePage.Service;

import adpro3.itsalad.gamePage.core.CheckScore;
import adpro3.itsalad.gamePage.core.Score;
import org.springframework.stereotype.Service;

@Service
public class DoneService {
    CheckScore cs;

    public DoneService(){
        this.cs= new CheckScore(new Score());
    }

    public void done(){
        this.cs.checker();
    }

    public int getScore(){
        return this.cs.getScore();
    }
}
